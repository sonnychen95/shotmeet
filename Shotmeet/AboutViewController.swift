//
//  AboutViewController.swift
//  Shotmeet
//
//  Created by Sonny Chen on 3/21/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit
import Parse

class AboutViewController: UIViewController {
    
    var photographer = PFUser.currentUser()
    
    @IBOutlet weak var aboutText: UITextView!
    
    @IBAction func updateUser(sender: AnyObject) {
        
           photographer!["bio"] = aboutText.text
            
            photographer!.saveInBackgroundWithBlock {
                (success: Bool, error: NSError?) -> Void in
                if (success) {
                    self.navigationController?.popToRootViewControllerAnimated(true)
                } else {
                    print("error")
                }

            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "About Me"
        
        if let about = photographer!["bio"] as? String {
            aboutText.text = about
        }
        
        
        
        
    }

}
