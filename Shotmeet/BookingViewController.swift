//
//  BookingViewController.swift
//  Shotmeet
//
//  Created by Sonny Chen on 3/16/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit
import Parse

class BookingViewController: UIViewController {
    
    var photographer: PFObject?
    
    @IBOutlet weak var bookingDescription: UITextView!
    @IBOutlet weak var contact: UITextField!
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func done(sender: AnyObject) {
        
        if bookingDescription.text == "" || contact.text == "" {
            let incomplete = UIAlertController(title: "Oops", message: "Please make sure everything is filled",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let OK = UIAlertAction(title:"OK",
                style:UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in
                    }
            incomplete.addAction(OK)
            self.presentViewController(incomplete, animated: true, completion: nil)
        
            } else {
            let request = PFObject(className: "Request")
            request.setValue(bookingDescription.text, forKey: "description")
            request.setValue(contact.text, forKey: "contact")
            request.setValue(photographer, forKey: "photographer")
            request.saveInBackgroundWithBlock {
                (success: Bool, error: NSError?) -> Void in
                if (success) {
                    let sent = UIAlertController(title: "Booking request sent!", message: nil,
                        preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let OK = UIAlertAction(title:"OK",
                        style:UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in
                            self.dismissViewControllerAnimated(true, completion: nil)
                    }
                   sent.addAction(OK)
                    self.presentViewController(sent, animated: true, completion: nil)
            }
            }}
               }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Tap
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)

        
        if let first = photographer!["first"] as? String {
        bookingDescription.text = "Hey " + first + "! Love your work and would love to set up a photoshoot."
        }
        
        let spacerView = UIView(frame:CGRect(x:0, y:0, width:10, height:10))

        contact.leftViewMode = UITextFieldViewMode.Always
        contact.leftView = spacerView
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

}
