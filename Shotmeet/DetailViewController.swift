//
//  DetailViewController.swift
//  Shotmeet
//
//  Created by Sonny Chen on 2/25/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit
import Parse

class DetailViewController: UIViewController, UICollectionViewDataSource {
    
    var photographer: PFObject?
    var profile = UIImage()
    var coverImage = UIImage()
    var objects : [PFObject] = []
    var photos = [UIImage]()
    var collectionViewLayout: CustomImageFlowLayout!
    var start = Int()
    var inclusionType = ["Price", "Duration", "Minimum Photos"]
    
    @IBOutlet weak var aboutPhotographer: UILabel!
    @IBOutlet weak var aboutText: UITextView!
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var portfolio: UICollectionView!
    @IBOutlet weak var photographerContainer: UIView!
    @IBOutlet weak var photographerPhoto: UIImageView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var packageTableView: UITableView!
    @IBOutlet weak var contact: UIButton!
    
    @IBAction func toggle(sender: AnyObject) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            portfolio.hidden = false
            aboutView.hidden = true
            packageTableView.hidden = true
        case 1:
            portfolio.hidden = true
            aboutView.hidden = false
            packageTableView.hidden = true
        case 2:
            portfolio.hidden = true
            aboutView.hidden = true
            packageTableView.hidden = false
        default:
            break; 
        }     }
    
    @IBAction func bottomContact(sender: AnyObject) {
        performSegueWithIdentifier("bookSegue", sender: self)
    }
    
    @IBAction func topContact(sender: AnyObject) {
        performSegueWithIdentifier("bookSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewLayout = CustomImageFlowLayout()
        portfolio.collectionViewLayout = collectionViewLayout
        
        getQuery()
        
        //Profile Picture
        photographerPhoto.image = profile
        let first = photographer!["first"] as? String
        let last = photographer!["last"] as? String
        
        //About
        aboutText.text = photographer!["bio"] as? String
        aboutPhotographer.text = "About " + first!
        
        self.navigationItem.title = first! + " " + last!
        photographerContainer.layer.cornerRadius = 40
        photographerContainer.layer.borderColor = UIColor.whiteColor().CGColor
        photographerContainer.layer.borderWidth = 4
        photographerContainer.clipsToBounds = true
        self.packageTableView.tableFooterView = UIView(frame: CGRect.zero)
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let packageCell = tableView.dequeueReusableCellWithIdentifier("packageCell") as! PackageTableViewCell!
        
        packageCell.inclusionType.text = inclusionType[indexPath.row]
        
        if indexPath.row == 0 {
            if let price = photographer!["price"] as? Int {
            packageCell.inclusionValue.text = "$ " + String(price)
            }
        } else if indexPath.row == 1 {
            if let duration = photographer!["duration"] as? Int {
                if duration == 1 {
                    packageCell.inclusionValue.text = String(duration) + " hr"
                
                } else {
                     packageCell.inclusionValue.text = String(duration) + " hrs"
                }
            }} else if indexPath.row == 2 {
                if let minimum = photographer!["minimumPhotos"] as? Int {
                    
                    packageCell.inclusionValue.text = String(minimum)
        }
            }
        
        packageCell.preservesSuperviewLayoutMargins = false
        packageCell.separatorInset = UIEdgeInsetsZero
        packageCell.layoutMargins = UIEdgeInsetsZero
        
        return packageCell
    }
    
    func getQuery() {
        
        SearchLoading().showLoading()
        let query = PFQuery(className: "Photos")
        query.findObjectsInBackgroundWithBlock {
            (let objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                self.objects = objects! as [PFObject]
                for object in self.objects {
                let photo = object["Photo"] as! PFFile
                photo.getDataInBackgroundWithBlock {
                    (imageData: NSData?, error: NSError?) -> Void in
                    if error == nil {
                        self.photos.append(UIImage(data:imageData!)!)
                        }
                     self.portfolio.reloadData()
                    }
                }
                }
        }
      
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "imageSegue"){
            let theDestination = segue.destinationViewController as! ImageGalleryViewController
            theDestination.pageImages = photos
            theDestination.start = start
            
        } else if (segue.identifier == "bookSegue") {
            let theNavigation = segue.destinationViewController as! UINavigationController
            let targetController = theNavigation.topViewController as! BookingViewController
            targetController.photographer = photographer!
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
         let photoCell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! PhotoCollectionViewCell
       
        photoCell.photo.image = photos[indexPath.row]
        SearchLoading().hideLoading()
    
        return photoCell
            
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
       
        start = indexPath.row
        performSegueWithIdentifier("imageSegue", sender: self)
    }


}
