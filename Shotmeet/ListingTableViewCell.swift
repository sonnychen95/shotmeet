//
//  ListingTableViewCell.swift
//  Shotmeet
//
//  Created by Sonny Chen on 2/25/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit

class ListingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var coverContainer: UIView!
    
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var profileContainer: UIView!
    
    
    @IBOutlet weak var subtitleDescription: UILabel!
    
    override func awakeFromNib() {
        
       
        self.coverContainer.clipsToBounds = true
        self.profileContainer.layer.cornerRadius = 23
        self.profileContainer.clipsToBounds = true
        self.profileContainer.layer.borderWidth = 1
        let borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.8)
        self.profileContainer.layer.borderColor = borderColor.CGColor
        
        
    }

}
