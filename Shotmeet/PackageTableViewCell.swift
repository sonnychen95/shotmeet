//
//  PackageTableViewCell.swift
//  Shotmeet
//
//  Created by Sonny Chen on 2/25/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit

class PackageTableViewCell: UITableViewCell {

    @IBOutlet weak var inclusionType: UILabel!
    @IBOutlet weak var inclusionValue: UILabel!
}
