//
//  PhotoCollectionViewCell.swift
//  Shotmeet
//
//  Created by Sonny Chen on 2/26/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {

       @IBOutlet weak var photo: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photo.image = nil
    }

}
