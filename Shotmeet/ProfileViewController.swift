//
//  ProfileViewController.swift
//  Shotmeet
//
//  Created by Sonny Chen on 2/26/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit
import Parse
import FBSDKLoginKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, FBSDKLoginButtonDelegate {
    
    let currentUser = PFUser.currentUser()
    
    let imagePicker = UIImagePickerController()
    var tableContent = ["About", "Photos", "Pricing", "Log Out"]
    
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var profilePictureContainer: UIView!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var signInView: UIView!
    @IBOutlet weak var profileView: UIView!
    
    
    @IBAction func requestSegue(sender: AnyObject) {
        performSegueWithIdentifier("requestSegue", sender: self)
    }
    
    @IBAction func signIn(sender: AnyObject) {
        
        PFUser.logInWithUsernameInBackground(usernameTF.text!, password: passwordTF.text!) { (user, error) -> Void in
            if error == nil {
                
                let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0, 0, 150, 150)) as UIActivityIndicatorView
                spinner.startAnimating()
                
                self.updateUser()
                
                spinner.stopAnimating()
                
            } else {
                let alert = UIAlertController(title: "Oops", message: "Try again", preferredStyle: UIAlertControllerStyle.Alert)
                let okButton = UIAlertAction(title:"OK",
                    style:UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in
                        self.dismissViewControllerAnimated(true, completion: nil)
                }
                alert.addAction(okButton)
                
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        if PFUser.currentUser() != nil {
            profileView.hidden = false
     
        } else {
            profileView.hidden = true
           
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUser()
        
        //Facebook
        var loginButton: FBSDKLoginButton = FBSDKLoginButton()
        loginButton.center = self.view.center
        self.view!.addSubview(loginButton)
        
        if let _ = FBSDKAccessToken.currentAccessToken() {
            fetchProfile()
        }
        
        
        //View Controller is delegate for imagePicker
        imagePicker.delegate = self
        
        //Tap
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        //Username
        let border1 = CALayer()
        let width = CGFloat(1.0)
        border1.borderColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0).CGColor
        border1.frame = CGRect(x: 0, y: passwordTF.frame.size.height - width, width:  passwordTF.frame.size.width, height: passwordTF.frame.size.height)
        border1.borderWidth = width
        
        usernameTF.layer.addSublayer(border1)
        usernameTF.layer.masksToBounds = true
        
        //Password
        let border2 = CALayer()
        border2.borderColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0).CGColor
        border2.frame = CGRect(x: 0, y: passwordTF.frame.size.height - width, width:  passwordTF.frame.size.width, height: passwordTF.frame.size.height)
        border2.borderWidth = width
        
        passwordTF.layer.addSublayer(border2)
        passwordTF.layer.masksToBounds = true
        
        //Profile
        profilePictureContainer.clipsToBounds = true
        profilePictureContainer.layer.cornerRadius = 50
        profilePictureContainer.layer.borderColor = UIColor.whiteColor().CGColor
        profilePictureContainer.layer.borderWidth = 4
        self.profileTableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func profpic() {
        
        let alert = UIAlertController(title: nil, message: nil,
            preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let camera = UIAlertAction(title:"Take Photo",
            style:UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .Camera
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        
        let library = UIAlertAction(title:"Choose Photo",
            style:UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .PhotoLibrary
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title:"Cancel",
            style:UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        alert.addAction(camera)
        alert.addAction(library)
        alert.addAction(cancel)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func updateUser() {
        
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            profileView.hidden = false
            
            if let pictureFile = currentUser!["profile"] as? PFFile {
                pictureFile.getDataInBackgroundWithBlock {
                    (imageData: NSData?, error: NSError?) -> Void in
                    if error == nil {
                        self.profilePicture.image = UIImage(data:imageData!)
                    }}}
            currentUser?.fetchInBackground()
            let first = currentUser!["first"] as? String
            let last = currentUser!["last"] as? String
            profileName.text = first! + " " + last!
            
        } else {
            profilePicture.image = nil
            profileView.hidden = true
            
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableContent.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("profileCell")! as UITableViewCell
        
        cell.textLabel!.text = tableContent[indexPath.row]
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row == 0) {
           
            performSegueWithIdentifier("aboutSegue", sender: self)
            
        } else if (indexPath.row == 1) {
            
        } else if (indexPath.row == 2) {
            
        } else if (indexPath.row == 3) {
            Logout()
        }

        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextProfile: UIImage = UIImage(CGImage: image.CGImage!)
        
        let contextSize: CGSize = contextProfile.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRectMake(posX, posY, cgwidth, cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextProfile.CGImage, rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(CGImage: imageRef, scale: image.scale, orientation: .Up)
        
        return image
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let pickedImage: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let scaledProfile = cropToBounds(pickedImage, width: 200, height: 200)
        let imageData = UIImageJPEGRepresentation(scaledProfile, 100)
        let imageFile:PFFile = PFFile(data: imageData!)!
        PFUser.currentUser()!.setObject(imageFile, forKey: "profile")
        PFUser.currentUser()!.saveInBackgroundWithBlock {
            (success, error) -> Void in
            if (success) {
                self.profilePicture.image = scaledProfile
            } else {
                print("error")
            }
        }
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    //navigation on image picker
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if navigationController.isKindOfClass(UIImagePickerController.self) {
            viewController.navigationController!.navigationBar.translucent = false
            viewController.edgesForExtendedLayout = .None
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
            
        }}
    
    func Logout() {
        
        let alertyo = UIAlertController(title: nil, message: nil,
            preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let logoutAction = UIAlertAction(title:"Logout",
            style:UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
                PFUser.logOut()
                self.updateUser()
        }
        let logoutCancel = UIAlertAction(title:"Cancel",
            style:UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertyo.addAction(logoutAction)
        alertyo.addAction(logoutCancel)
        self.presentViewController(alertyo, animated: true, completion: nil)
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        fetchProfile()
    }
    
    func fetchProfile() {
        let parameters = ["fields": "email, first_name, last_name, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).startWithCompletionHandler({ (connection, user, requestError) -> Void in
            
            if requestError != nil {
                print(requestError)
                return
            }
            
            var email = user["email"] as? String
            let firstName = user["first_name"] as? String
            let lastName = user["last_name"] as? String
            
            var pictureUrl = ""
            
            if let picture = user["picture"] as? NSDictionary, data = picture["data"] as? NSDictionary, url = data["url"] as? String {
                pictureUrl = url
            }
            
        })
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }

func dismissKeyboard() {
    view.endEditing(true)
}

}
