//
//  RequestTableViewCell.swift
//  Shotmeet
//
//  Created by Sonny Chen on 3/27/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit

class RequestTableViewCell: UITableViewCell {

    @IBOutlet weak var bookingMessage: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    @IBOutlet weak var bookerName: UILabel!

}
