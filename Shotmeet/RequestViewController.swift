//
//  RequestViewController.swift
//  Shotmeet
//
//  Created by Sonny Chen on 3/28/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit
import Parse

class RequestViewController: UIViewController {
    
    var requests = [PFObject]()
    
    @IBOutlet weak var requestTableView: UITableView!
    @IBOutlet weak var emptyState: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emptyState.hidden = true
        getQuery()
        
        self.requestTableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    
    func getQuery() {
        
        let query = PFQuery(className: "Request")
        query.whereKey("photographer", equalTo: PFUser.currentUser()!)
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                self.requests = objects!
            }
            self.requestTableView.reloadData()
            
            if self.requests.count == 0 {
                self.emptyState.hidden = false
            } else {
                self.emptyState.hidden = true
            }
            
        }
        
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return requests.count
    }
    
   func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let requestCell = tableView.dequeueReusableCellWithIdentifier("requestCell") as! RequestTableViewCell!
        
        
        requestCell.preservesSuperviewLayoutMargins = false
        requestCell.separatorInset = UIEdgeInsetsZero
        requestCell.layoutMargins = UIEdgeInsetsZero
        
        let request = requests[indexPath.row]
    
    
    if let name = request["bookerName"] as? String {
        requestCell.bookerName.text = name
    }
    
        
        if let message = request["description"] as? String {
            requestCell.bookingMessage.text = message
        }
        
        return requestCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //photographer = objects[indexPath.row]
        
        //let cell = tableView.cellForRowAtIndexPath(indexPath) as! ListingTableViewCell
        //coverImage = cell.cover.image!
        //profile = cell.profile.image!
        
        //performSegueWithIdentifier("detailSegue", sender: self)
        //tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    


}
