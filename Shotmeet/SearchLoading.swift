//
//  SearchLoading.swift
//  Shotmeet
//
//  Created by Sonny Chen on 2/27/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit

class SearchLoading {

var loadingView = UIView()
var container = UIView()
var activityIndicator = UIActivityIndicatorView()
var bluegreen = UIColor(red: 75.0/255.0, green: 195.0/255.0, blue: 180.0/255.0, alpha: 1.0)

func showLoading() {
    
    let win:UIWindow = UIApplication.sharedApplication().delegate!.window!!
    self.loadingView = UIView(frame: win.frame)
    self.loadingView.tag = 1
    self.loadingView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0)
    
    win.addSubview(self.loadingView)
    
    container = UIView(frame: CGRect(x: 0, y: 0, width: win.frame.width/3, height: win.frame.width/3))
    container.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6)
    container.layer.cornerRadius = 10.0
    container.clipsToBounds = true
    container.center = self.loadingView.center
    
    
    activityIndicator.frame = CGRectMake(0, 0, win.frame.width/5, win.frame.width/5)
    activityIndicator.activityIndicatorViewStyle = .WhiteLarge
    activityIndicator.color = bluegreen
    activityIndicator.center = self.loadingView.center
    
    
    self.loadingView.addSubview(container)
    self.loadingView.addSubview(activityIndicator)
    
    activityIndicator.startAnimating()
    
}

func hideLoading(){
    UIView.animateWithDuration(0.0, delay: 1.0, options: .CurveEaseOut, animations: {
        self.container.alpha = 0.0
        self.loadingView.alpha = 0.0
        self.activityIndicator.stopAnimating()
        }, completion: { finished in
            self.activityIndicator.removeFromSuperview()
            self.container.removeFromSuperview()
            self.loadingView.removeFromSuperview()
            let win:UIWindow = UIApplication.sharedApplication().delegate!.window!!
            let removeView  = win.viewWithTag(1)
            removeView?.removeFromSuperview()
    })
}

}
