//
//  SearchViewController.swift
//  Shotmeet
//
//  Created by Sonny Chen on 2/23/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit
import Parse

class SearchViewController: UIViewController, UIPickerViewDelegate {
    
    var bluegreen = UIColor(red: 75.0/255.0, green: 195.0/255.0, blue: 180.0/255.0, alpha: 1.0)
    var objects: [PFObject] = []
    var photographer: PFObject?
    var profile = UIImage()
    var coverImage = UIImage()
    var cities = ["Boston", "Chicago", "Los Angeles", "New York", "San Francisco Bay Area", "Seattle", "Washington DC"]
    var selectedCity = String()
    

  
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var location: UIPickerView!

    
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var listingTableView: UITableView!

    
    @IBAction func pickLocation(sender: AnyObject) {
        if locationView.hidden == true {
        findIndex()
        locationView.hidden = false
        locationButton.tintColor = bluegreen
        
        } else {
        locationView.hidden = true
        locationButton.tintColor = UIColor.whiteColor()
           
        }
    }
    
    @IBAction func locationDone(sender: AnyObject) {
        if selectedCity == "San Francisco Bay Area" {
        locationView.hidden = true
        locationButton.tintColor = UIColor.whiteColor()
        getQuery()
        } else {
            let alert = UIAlertController(title: "Coming Very Soon!", message: "Sorry, we currently only serve San Francisco Bay Area",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let OKaction = UIAlertAction(title:"OK",
                style:UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in
                    self.dismissViewControllerAnimated(true, completion: nil)
            }
            alert.addAction(OKaction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        selectedCity = "San Francisco Bay Area"
        locationView.hidden = true

        getQuery()
        
        self.listingTableView.tableFooterView = UIView(frame: CGRect.zero)

        }
    
    
    
    func getQuery() {
        
        SearchLoading().showLoading()
        
        let query = PFQuery(className: "_User")
        query.whereKey("location", equalTo: selectedCity)
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                self.objects = objects!
            }
            self.listingTableView.reloadData()
            
        }
        self.locationButton.setTitle(" " + selectedCity, forState: .Normal)
        
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == listingTableView {
        return objects.count
        } else {
            return 5
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
        if tableView == listingTableView {
            
        let listingCell = tableView.dequeueReusableCellWithIdentifier("listingCell") as! ListingTableViewCell!

        
        listingCell.preservesSuperviewLayoutMargins = false
        listingCell.separatorInset = UIEdgeInsetsZero
        listingCell.layoutMargins = UIEdgeInsetsZero
        
        let photographer = objects[indexPath.row]
        
        if let profile = photographer["profile"] as? PFFile {
            profile.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    listingCell.profile.image = UIImage(data:imageData!)
                }}}
        
        if let cover = photographer["cover"] as? PFFile {
            cover.getDataInBackgroundWithBlock {
                (imageData: NSData?, error: NSError?) -> Void in
                if error == nil {
                    listingCell.cover.image = UIImage(data:imageData!)
                }}}
            
            if let first = photographer["first"] as? String {
                if let last = photographer ["last"] as? String {
            listingCell.subtitleDescription.text = first + " " + last
            }}
        
        cell = listingCell
        
        }
        SearchLoading().hideLoading()
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        photographer = objects[indexPath.row]
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ListingTableViewCell
        coverImage = cell.cover.image!
        profile = cell.profile.image!

        performSegueWithIdentifier("detailSegue", sender: self)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "detailSegue"){
            let theDestination = segue.destinationViewController as! DetailViewController
            theDestination.photographer = photographer!
            theDestination.profile = profile
            theDestination.coverImage = coverImage
        }
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cities.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cities[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedCity = cities[row]
        
    }
    
    func findIndex() {
        let placement = cities.indexOf(selectedCity)
        location.selectRow(placement!, inComponent: 0, animated: false)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offsetY = self.listingTableView.contentOffset.y
        for cell in self.listingTableView.visibleCells as! [ListingTableViewCell] {
            let x = cell.cover.frame.origin.x
            let w = cell.cover.bounds.width
            let h = cell.cover.bounds.height
            let y = ((offsetY - cell.frame.origin.y) / h) * 10
            cell.cover.frame = CGRectMake(x, y, w, h)
        }
    }

    
    
    
}
