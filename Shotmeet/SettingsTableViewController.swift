//
//  SettingsTableViewController.swift
//  Shotmeet
//
//  Created by Sonny Chen on 3/31/16.
//  Copyright © 2016 Shotmeet. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
       override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Settings"
        self.tableView.tableFooterView = UIView()
        
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Futura-Medium", size: 13)!
        header.textLabel?.textColor = UIColor.grayColor()
        let headerColor: UITableViewHeaderFooterView = (view as! UITableViewHeaderFooterView)
        headerColor.backgroundView!.backgroundColor = UIColor.groupTableViewBackgroundColor()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
            } else if indexPath.row == 1 {
                let alertController = UIAlertController(title: "Found something?", message:
                    "Send feedbacks to feedback@shotmeet.com", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        } else if indexPath.section == 1 {
            
            if indexPath.row == 0  {
                let alertController = UIAlertController(title: "Have What it Takes?", message:
                    "Apply at www.Shotmeet.com", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }


}
